SubGit 3.3.9
Build #4351 published on Oct 30, 2019}
====================================

Fixed Bugs:

* Possible NPE in JGit library initialization, fixed.

Improvements:

* When shared daemon is used, mirror repositories polls are distributed evenly
  along fetch interval time to avoid load spikes.

SubGit 3.3.8
Build #4337 published on 25 Sep 2019
====================================

Fixed Bugs:

* SubGit metadata might be corrupted and lead to translation errors in case of file system access errors, fixed.
* 'subgit configure' might freeze when running on certain file systems, fixed (https://issues.tmatesoft.com/issue/SGT-1308)
* Possible NPE on 'subgit install --rebuild-from-revision' command, fixed.

New Features:

* 'svn.logWindowSize' config option added to control SVN log command batch window size;
   increase to improve initial configurtation and translation performance for huge SVN repositories.
* 'svn.useSubGitLock' config option added;
   set to 'false' to prevent SubGit from setting subgit:lock SVN property on SVN branches and tags.

SubGit 3.3.7
Build #4325 published on 10 Jul 2019
====================================

Fixed Bugs:

* NPE when using relative paths fixed (https://issues.tmatesoft.com/issue/SGT-1303)
* Invalid jar file Manifest.MF signature fixed (https://issues.tmatesoft.com/issue/SGT-1304)
* GitLab 12 compatibiliy issue fixed (https://issues.tmatesoft.com/issue/SGT-1302)
* Support for .. in .gitignore patterns (https://issues.tmatesoft.com/issues/SGT-1301)

Improvements:

* Git core.trustfolderstat option is no longer set to 'false' by defaut to avoid performance issues.
  'false' value of this option is reverted in case it was set explicitly by previous versions of SubGit.
* Other stability and performance improvements.

New Features:

* New SubGit option svn.useSubGitLock introduced. When set to 'false', SubGit will not
  modify subgit:lock property as part of its SVN commits.

SubGit 3.3.6
Build #4276 published on 15 Mar 2019
====================================

Fixed Bugs:

* 'subgit install --rebuild' didn't work on Windows (https://issues.tmatesoft.com/issue/SGT-1259)
* 'subgit install --refetch' improvements (https://issues.tmatesoft.com/issues/SGT-274, https://issues.tmatesoft.com/issues/SGT-1264)
* refs/svn/history/first should not contain authors with empty names (https://issues.tmatesoft.com/issue/SGT-1282)
* Git user hooks should be preserved on 'install --rebuild' in local mirror mode (https://issues.tmatesoft.com/issue/SGT-1268)
* Double quotes in JSON output fixed (https://issues.tmatesoft.com/issue/SGT-1287)
* Fixed incorrect SVN/Git translation of paths that include ASCII control characters (https://issues.tmatesoft.com/issue/SGT-1278)

New Features:

* core.mapGitCommitter boolean option supported to fine tune Git to SVN authors mapping:
  map either Git commit author or committer. Default is to use Git committer.

Improvements:

* Improved performance for large Git pack files handling
* Support for 0 revision value as --rebuild-from-revision option argument
* Support for SVN 1.10 FSFS repository format (https://issues.tmatesoft.com/issue/SGT-1260)
* Other stability improvements

SubGit 3.3.5
Build #4042 published on 20 Oct 2018
====================================

Improvements:

* Improved binary files handling by subgit 'apply-patch' command.
* Subgit daemon port could be explicitly speicifed with the daemon.port configuration option.

SubGit 3.3.4
Build #4034 published on 27 Sep 2018
====================================

Fixed Bugs:

* Fixed issue with GitLab UI and CI/CD jobs when SubGit fethes new changes from Subversion repository:
  https://issues.tmatesoft.com/issue/SGT-1267

New Features:

* Shared daemon REST API:
  https://subgit.com/documentation/rest.html
* subgit apply-patch command introduced that allows applying patches of the specified format to SubGit mirrors;
  see `subgit help apply-patch`.
* Support for Subversion 1.10 repository format over file:// protocol.
* Support for core.rejectUnmappedGitAuthor option introduced that allows to reject Git commits with committers
  not specified in the authors mapping file.
* Support for svn.refWhitespaceReplacement option introduced that allows to specify string replacement of a whitespace
  in Subversion branches or tags.

Improvements:

* Author mapping now relies on 'committer' field of a given Git commit rather than 'author'.
* SubGit now rejects push operations that exceed Git committers limit set by a license key.
* When counting Git users against a license key, SubGit treats two different Git committers mapped to the same SVN
  author as a single user.
* Other stability improvements.

SubGit 3.3.3
Build #3877 published on June 7, 2018
====================================

Fixed Bugs:

* When pushing files of size > 50Mb and Git version is >= 2.11.0, they got translated to empty files in SVN.
  See more details at https://issues.tmatesoft.com/issue/SGT-1250.
* Potential corruption when translating files of size > 50Mb could result into "Checksum mismatch" error.

SubGit 3.3.2
Build #3875 published on May 22, 2018
====================================

Fixed Bugs:

* SubGit now rejects push operations that update refs with non-ASCII characters.
  See more details at https://issues.tmatesoft.com/issue/SGT-1243.
* Fixed possible NPE when SVN server relies on HTTP digest authentication scheme.
* Fixed potential dead-lock when working through file:// protocol on Linux with older
  version of 'flock' utility.

New Features:

* subgit install command now has exclusive access to repository, so no other SubGit processes can
  interfere with installation process.
* subgit map command now supports --json and --paths-file options.
* subgit install command does not allow deleting SVN branches and Git refs.
* Default evaluation period is 7 days; it is now possible to extend trial at https://subgit.com/pricing.

Improvements:

* 'subgit install --refetch' command made more efficient by resetting mirror to a more recent revision.
* Synchronization performance improved on SVN repositories with many revisions mapped to Git commits.

SubGit 3.3.1
Build #3318 published on April 3, 2018
====================================

Fixed Bugs:

* Fixed "IllegalThreadStateException: process hasn't exited" error.
  See more details at https://issues.tmatesoft.com/issue/SGT-1219.
* Fixed potential repository corruption when working through file:// protocol on Linux when
  interactiing with native Subversion is compiled to use FLOCK instead of POSIX locks.
  The problem happened when native Subversion commits changes to the SVN repository and SubGit
  translates Git commits to SVN revisions at the same time.
  See more details at https://issues.tmatesoft.com/issue/SVNKIT-719.
* Out-of-memory error fixed for huge Git repositories with huge pack files and pack indexes.
  See more details at https://issues.tmatesoft.com/issue/SGT-1237.
* Fixed process handle leak on Windows.
* Better error message when pushing via SSH and user running SubGit can't access
  the uploaded objects. Original error message was "Missing unknown <SHA-1>".
  See more details at https://issues.tmatesoft.com/issue/SGT-1231.
* Fixed Java version check for JRE 10.
* When 'shelves=' option is not specified any "subgit install" command ran long unnecessary
  configuration validation that could last several hours on large repository
  (even if such "subgit install" whould be no-op).
  See more details at https://issues.tmatesoft.com/issue/SGT-1224
* Fixed false detection of external changes. In some cases when someone committed to SVN near the
  moment of creating new SVN revision by SubGit that lead to the message about potentially corrupted
  repository even if the repository was fine.
  See more details at https://issues.tmatesoft.com/issue/SGT-1230

Other:

* Java 1.8 or newer is required.

SubGit 3.3.0
Build #3771 published on Jan 19, 2018
====================================

New Featrues:

* subgit verify command introduced that checks the consistency of a mirror; it allows
  to identify Subversion revisions that are missing in Git repository.
* Upgrade from version 3.2.7 or older automatically runs `subgit verify` on a mirror.
* Minor usability improvements.

Fixed Bugs:

* Fixed potential data loss in Git repository on concurrent changes from both SVN and Git sides
  with Subversion server 1.9.x.
  See more details at https://issues.tmatesoft.com/issue/SGT-1208.
* Fixed potential data loss in Git repository on concurrent changes from both SVN and Git sides
  when SubGit updates one branch and deletes another one within a single revision.
  See more details at https://issues.tmatesoft.com/issue/SGT-1212.
* Minor usability improvements.

SubGit 3.2.7
Build #3729 published on Nov 2, 2017
====================================

Fixed Bugs:

* Fixed potential data loss in Git repository on concurrent changes from both SVN and Git sides
  with Subversion server 1.9.x.
  See more details at https://issues.tmatesoft.com/issue/SGT-1208.
* Fixed potential data loss in Git repository on concurrent changes from both SVN and Git sides
  when SubGit updates one branch and deletes another one within a single revision.
  See more details at https://issues.tmatesoft.com/issue/SGT-1212.
* Minor usability improvements.

SubGit 3.2.6
Build #3714 published on Aug 16, 2017
====================================

New Features:

* Branches mapping patterns support multiple asterisks within single segment.

Fixed Bugs:

* Configuration option auth.sshKeyFilePassphrase was not read, fixed.
* Suboptimal initial translation performance in case of single-directory mirror, fixed.
* Various performance improvements.

SubGit 3.2.5
Build #3694 published on Jun 16, 2017
====================================

New Featrues:

* subgit map command introduced that allows to query
  effective branches and authors mappings set up in a mirror repository.

SubGit 3.2.4
Build #3670 published on Feb 13, 2017
====================================

New Features:

* Support for svn.allowReplacementBranches and svn.allowDeletionBranches options added that allow
  to control SVN branch replacement and deletion translation policy for individual branches.
* Support for svn.gitCommitMessage and svn.svnCommitMessage options that allow specifying message
  patterns used by SubGit while generating commit messages.

Fixed Bugs:

* When non-translatable Git commit is synced to SVN and creation of empty SVN revisions is disabled,
  it might happen that non-translatable Git commit would be lost on later syncs, fixed.
* Potential minor memory leak on failed sync, fixed.

SubGit 3.2.3
Build #3645 published on Jan 18, 2017
====================================

New Features:

* Support for recursive pattern as svn.includePath option value.
* Git gc is now called periodically on SVN to Git translation to ensure
  Git repository objects are packed.
* Support for temporary Git object directories introduced in Git 2.11.

Fixed Bugs:

* Improved command line help output for some of the commands.
* Potential SVN repository corruption, when writing to FSFS SVN repository
  stored in SVN 1.9 format, fixed.

SubGit 3.2.2
Build #3603 published on Aug 15, 2016
====================================

Fixed Bugs:

* Registration expiration date was handled incorrectly in version 3.2.1, fixed.

SubGit 3.2.1
Build #3593 published on July 22, 2016
====================================
New Features:

* Option svn.allowBranchReplacement added, defaul is to disable SVN branch replacement when
  translating Git commits ot SVN.

Fixed Bugs:

* Improved handling of situations when reference is locked, but lock is no longer valid.
* Translation of SVN replacing symbolic link with a directory did not work properly, fixed.

SubGit 3.2.0
Build #3512 published on May 16, 2016
====================================
New Features:

* Improved GitLab integration, see http://subgit.com/gitlab for HOWTO.
* Support for new 1.9 Subversion FSFS repository format.
* Improved unsynced commits handling.

Fixed Bugs:

* Client SSL certificates without passphrase were not supported, fixed.
* Push of two branches deletion might result in out-of-date error, fixed.
* Minor stability improvements.

SubGit 3.1.2
Build #3458 published on March 15, 2016
====================================
New Features:

* SGT-1076: specify timezone to use in Git commits (default is UTC).

Other Changes:

* JGit Library updated to v4.2.0


SubGit 3.1.1
Build #3488 published on Feb 10, 2016
====================================
Fixed bugs:
* SGT-1056: Branch deletion in Git may overwrite changes in SVN on the same branch, fixed.
* Improvements in conflicts recovery code.

SubGit 3.1.0
Build #3419 published on Dec 19, 2015
====================================
New features:

* SubGit could now be licensed for a number of Git user, i.e. number of user that push to Git/SVN mirror using Git.
  With Git users registration number of SVN committers is not taken into account.

* Evaluation period is extended to 30 days.
* Sync will be now disabled upon evaluation period expiration.

Fixed bugs:

* Stability and performance improvements.

Other:

* Java 1.7 or newer is required.

SubGit 3.0.0
Build #3320 published on Jul 31, 2015
====================================
New Features:

* Single-directory Subversion projects support.
* 'configure' command (initial mirror configuration command) improved with the following options:

   --layout auto --trunk trunkPath : detects project branches and generates configuration;
   --layout dir : generates single-directory mirror configuration;
   --layout std : generates default trunk/branches/tags configuration.

* Shelves (anonymous Git branches) mirroring is now optional.
* Improved wildcard mapping syntax support, the following mappings are valid now:

  branches = branches/*/*:refs/heads/*/*
  branches = branches/*/project:refs/heads/*
  branches = branches/feature_*_branche:refs/heads/feature_*

* Patterns could be specified to exclude (or include) particular paths or branches from translation.
* Credential helper support allows to configure custom script or program to provide Subversion credentials.
* Authors mapping could be configured to use custom script or program to provide Subversion/Git authors mapping.
* Upon Subversion to Git translation user-post-receive hook script might be configued to be called.
* Dedicated shared daemon background process that manages multiple Git mirrors could be now configured
  in place of a single background process per Git mirror.
* Automated restoration from the 'out of sync' mirror state.

Fixed bugs:

* Performance issues related to the large binary objects processing fixed.

SubGit 2.0.3
Build #2796 published on Sep 18, 2014
====================================
New Features:

* pre-revprop-change hook script is no longer required for authors mapping to work.
* "subgit configure --svn-url <URL>" works without explicit target repository path,
  similar to 'git clone' command.

Fixed Bugs:

* SGT-896: Updates to authors.txt should be effective immediately, not after 'subgit install'.
* SGT-864: Import command can now continue, if interrupted.
* multiple translation stability fixes.
* failure to update single reference should not prevent other updates.
* initial translation performance improved.

SubGit 2.0.2
Build #2731 published on Mar 10, 2014
====================================
New Features:

* SGT-837 : Introduced pathEncoding value "none". When it is specified, no %-encoding is performed while translation file paths.

Fixed Bugs:

* SGT-838: Import command ignored ssl key path entered from keyboard.
* SGT-835: "Path 'XXX' not present" error while push, if a directory with all children skipped was renamed in Git.
* SGT-835: "File already exists" error while push, if a directory with .gitignore was renamed in Git
           and this .gitignore contains patterns corresponding to empty directories.
* SGT-834: If SVN author is an e-mail address, don't add "@defaultDomain" to get Git author's e-mail.
* SGT-831: On deletion "skip" patterns were applied to project-relative paths, not branch-relative.
* SGT-825: Push translation fails if ".gitignore" file is renamed to any other file.

SubGit 2.0.1
Build #2706 published on Dec 26, 2013
====================================
New Features:

* SGT-780 : support Subversion 1.8 repositories in local mirror mode.
* SGT-772 : support partial layout update on the fly.
* SGT-781 : SSH-agent support.

Fixed Bugs:

* Stability and performance improvements.

SubGit 2.0.0-RC1
Build #2593 published on Jul 24, 2013
====================================

 * 2.0.0 Release Candidate version.

SubGit 2.0.0-EAP5
Build #2552 published on May 30 2013
====================================
New Features:

 * SGT-81 : core.defaultDomain option to specify domain name to use
            for email composition in absence of explicit authors mapping.
 * SGT-586: Options added to specify default Subversion configuration directory
            to look for credentials.
 * SGT-588: Improved console feedback on Git push.

Fixed Bugs:

 * SGT-648: Improved background process detection algorithm to avoid multiple
            background processes launch.
 * SGT-659: Same translation shared by multiple Git repositories were not supported.
 * SGT-630: minimalRevision option support was broken in remote repository mirror mode.
 * SGT-640: Exception on attempt to push to unmapped Git branch.
 * SGT-639: Configure command failed with JRockit JVM.

SubGit 2.0.0-EAP3
Build #2208 published on Feb 2 2013
====================================
New Features:
 * Native hook executables are used whenever possible to improve performance
   and reduce translation overhead.

Fixed Bugs:

 * SGT-597   Default timeout values increased, too short ones caused interim failures.
 * SGT-594   In certain cases pushing new branch or branch deletion from Git resulted in OOD error.
 * SGT-547   It is now possible to configure non-empty Git repository to be a mirror
             of a remote Subversion one (assuming Subversion project does not yet exist).

 * Numerous minor stability improvements.

SubGit 2.0.0-EAP2
Build #2018 published on Dec 17 2012
====================================
New Features:
 * Writable Git mirror of a remote Subversion repository;
   see 'subgit help configure' and 'subgit help install' for details.

SubGit 1.0.2
Build #1764 published on Oct 15 2012
====================================
Fixed Bugs:
 * SGT-530  Potential failure on translating deletion of the 'trunk' directory.
 * SGT-531  File handlers leak on SubGit installation stage (in case of a huge repository).
 * SGT-533  JDK 1.7 specific exception on Git to Subversion translation: comparison method violates its general contract.

SubGit 1.0.1
Build #1696 published on Oct 3 2012
====================================
Fixed Bugs:
 * Wrong order of options in subgit.conf configuration file (SGT-526) fixed.
 * Potential BUSY error during translation (SGT-518) fixed.
 * Copied 'branches' directory and use of --minimal-revision option might result in failed translation, fixed.
 * In case Git update hook is present it should not be disabled during SubGit installation (SGT-517).

SubGit 1.0.0
Build #1696 published on Sep 19 2012
====================================
Improvements:
 * Documentation extended

SubGit 1.0.0-RC2
Build #1546 published on Jul 28, 2012
=====================================
Fixed Bugs:
 * Windows UNC-style paths supported
 * Stability fixes in Git to Svn translation engine

SubGit 1.0.0-RC1
Build #1519 published on Jul 16, 2012
=====================================
New Features:
 * Registration support added

Fixed Bugs:
 * Stability fixes

SubGit 1.0.0-EAP5
Build #1096 published on Feb 13, 2012
=====================================
New features:
 * Command option added: 'configure --minimal-revision'
 * Command option added: 'uninstall --purge'
 * User hooks are ran when present
 * Config options added: 'hooks.javaOptions' and 'daemon.javaOptions'
 * Daemon idle timeout option made configurable

Improvements:
 * Improved file permissions issues handling

Fixed Bugs:
 * Invalid error report created on crash
 * 'pre-commit' hook error did not block commit on Windows XP/Server 2003
 * Daemon process failed to exit
 * Checksum mismatch on svn:eol-style/.gitattributes modification


SubGit 1.0.0-EAP4
Build #902 published on Dec 20, 2011
====================================
New features:
 * Debian package distribution

Improvements:
 * Daemon launch time decreased
 * Translation: svn:ignore <=> .gitignore

Fixed Bugs:
 * 'Cannot assign requested address' error
 * Modification of svn:ignore removed empty directories
 * Install failed to upgrade libraries
 * JVM arguments in hook scripts on Windows


SubGit 1.0.0-EAP3
Build #789 published on Nov 19, 2011
====================================
New Features:
 * Multiple Git repositories support
 * Commands added: 'configure', 'install', 'uninstall'
 * Command option added:  'install --rebuild'
 * Config option added: 'git.pathEncoding'
 * Repository layout detection

Improvements:
 * Better error reporting

Fixed Bugs:
 * Possible dead-lock in case of concurrent access
 * Multiple Daemon processes launched for a single Svn repository


SubGit 1.0.0-EAP2
Build #493 published on Sep 16, 2011
====================================
New Features:
 * Commit time ordering for Git to Svn translation
 * Config option added: 'core.shared'

Improvements:
 * Git to Svn translation performance
 * File permissions issues handling

Fixed Bugs:
 * Invalid message prefix in branch name
 * Branch/tag creation commit time


SubGit 1.0.0-EAP1
Build #387 published on Aug 30, 2011
====================================
 * Basic single Subversion/Git repository support
